# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 20:10:48 2015

@author: ilija
"""

import numpy as np
import matplotlib.pyplot as plt
#definirano polje od 100 elemenata
velicina=[100]
#funkcija za generiranje random brojeva
slucajni_odabir=np.random.choice([1,2,3,4,5,6],velicina)
print "brojevi:", slucajni_odabir
#crtanje histograma
plt.hist(slucajni_odabir, bins=[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5])
