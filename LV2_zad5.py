# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 21:30:13 2015

@author: ilija
"""

import pandas as pd
import matplotlib.pyplot as plt
#izlistavanje automobila
mtcars = pd.read_csv('mtcars.csv')
print len(mtcars)
print mtcars[['car','mpg','hp']]
#iscrtavanje dijagrama
plt.plot(mtcars.mpg)#plava boja "potrošnja"
plt.plot(mtcars.hp)#zelena "konjska snaga"
plt.plot(mtcars.wt)#crvena "težina"

